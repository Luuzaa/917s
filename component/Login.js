import React , {useState} from 'react';
import {View, Text, StyleSheet, Image, Alert} from 'react-native';
import InputComponent from './InputComponent';
import CustomButtonComponent from './CustomButtonComponent';

function LoginScreen({navigation}) {

    const createRegister = () => {
        navigation.navigate('Register')
    }

    const signInPress = () => {
         console.warn('Sign in click')
         navigation.navigate('HomeScreen')
    }
    return (  
        <View style={styles.container}>
             <Image source={{uri: 'https://images.unsplash.com/photo-1515879218367-8466d910aaa4?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=869&q=80'}} style={styles.logoImage}/>
             <InputComponent placeName = 'UserName'/>
             <InputComponent placeName= 'Password'/>
             <CustomButtonComponent text='Sign in' type='Sign' onPress={signInPress} />
             <CustomButtonComponent text='Forget Password' type='Forget' />
             <CustomButtonComponent text='Sign in with Google' type='Google' />
             <CustomButtonComponent text='Sign in with Facebook' type='Facebook' />
             <CustomButtonComponent onPress={createRegister} text='Dont have account? Create one' type='Create' />
        </View>
    );
}

const styles = StyleSheet.create({
    logoImage: {
        width: '100%',
        height: 200
    },
    container: {
       flex:1,
    //    backgroundColor: 'red',
       alignItems: 'center'
    }
})
export default LoginScreen;