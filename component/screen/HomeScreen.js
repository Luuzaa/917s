import React, {useState, useEffect} from 'react';
import { View, StyleSheet, Text, FlatList, TouchableOpacity , Image, SafeAreaView} from 'react-native';
import { categories } from '../Data/Data';
function HomeScreen() {
   const [data, setData] = useState()

   useEffect(() => {
      setData(categories)
   }, [])
   
    return ( 
        <SafeAreaView>
             <FlatList 
            //    horizontal 
               numColumns={2}
               data={data}
            //    showsVerticalScrollIndicator = {false}
            //    showsHorizontalScrollIndicator = {false}
               keyExtractor = {(item) => item.id}
               renderItem = {({item}) => (
                <View>
                     <Text style={{fontSize:20, padding: 30}}>{item?.name}</Text>
                </View>
                )}
             />
        </SafeAreaView>
     );
}

export default HomeScreen;