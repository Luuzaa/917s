import React, { useState } from 'react'
import { View, Text, Button, StyleSheet} from 'react-native'
function Name({ user, caption, password }) {

    const [passHistory, setPassHistory] = useState([])
    const viewPass = () => {
        setPassHistory([...passHistory, password])
    }
    console.log('password--->', password)
    return (
        <View>
                <Text style={styles.name}>
                        {caption}:{user}
                </Text>
                {
                    passHistory.map((item, index) => {
                        return(
                            <Text key={index} style={styles.passText}>
                                {caption}:{item}
                            </Text>
                        )
                    })  
                }
                {/* {password &&  <button  onClick={viewPass} >Хадгалах</button> } */}
                {
                    password &&  <Button 
                    style={styles.button}
                    onPress={viewPass}
                    title="Хадгалах"
                    color="black"
                  />
                }
        </View>
    );
}

export default Name;


const styles = StyleSheet.create({
    name: {
        fontSize: 30
    },
    passText: {
        fontSize: 30,
        // color: 'b'
    },
    button: {
        fontSize: 30
    }
})