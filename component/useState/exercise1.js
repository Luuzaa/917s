import React, {useState}from 'react'
import { View, Text, Button, StyleSheet } from 'react-native'

function Exercise1() {
    const[count, setCount] = useState(5)
    const plus = () => {
        setCount(count +1)
    }

    const minus = () => {
        setCount(count -1)
    }
    return ( 
        <View style={styles.container}>
              <Text style={styles.text}>
                   Count value is: {count}
              </Text>
              <Button 
                style={styles.button}
               onPress={() => setCount(0)}
               title="Reset"
               color="#841584"
              />
              <Button 
                style={styles.button}
                onPress={plus}
               title=" Plus (+)"
               color="#841584"
              />
              <Button 
                style={styles.button}
                onPress={minus}
                title="Minus (-)"
                color="#841584"
              />
        </View>
    );
}

export default Exercise1;

const styles = StyleSheet.create({
      button: {
        fontSize: 36
      },
      container: {
        backgroundColor: 'yellow',
        // flex:1
        flexDirection: 'column'
        // display: 
      },

      text: {
        fontSize: 30
      }
})