import React, {useState}from 'react'
import { View, Text, Button, StyleSheet, TextInput } from 'react-native'
import Name from './name'
function Exercise3() {
    const [userName, setUserName] = useState('')
    const [password , setPassword] = useState('')
    const changeName = (e) => {
        setUserName(e)
    }


    const changePassword = (e) => {
        setPassword(e)
         
    }
    return ( 
        <View style={styles.container}>
                <Name caption='Name' user = {userName}/>
            {/* <input type="text"  onChange={changeName}/> */}
            <TextInput
              style={styles.input}
              placeholder= 'Хэрэглэгчийн нэр'
              value={userName}
              placeholderTextColor='white'
              onChangeText={(text) => changeName(text)}
            />
            <Name caption='Password' password = {password}/>
            <TextInput
              style={styles.input}
            //   keyboardType='number-pad'
              secureTextEntry={true}
              placeholder= 'Хэрэглэгчийн нууц үг'
              value={password}
              placeholderTextColor='white'
              onChangeText={(text) => changePassword(text)}
            />
        </View>
    );
}

export default Exercise3;

const styles = StyleSheet.create({
      button: {
        fontSize: 36
      },
      container: {
        backgroundColor: 'limegreen',
        // flex:1
        // display: 
      },

      text: {
        fontSize: 30
      },
      input: {
        // color: 'red',
        marginHorizontal: 30,
        paddingVertical: 10,
        borderRadius: 10,
        backgroundColor: 'gray',
        fontSize: 20
      }
})