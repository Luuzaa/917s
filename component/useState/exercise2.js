import React, {useState} from 'react'
import { View, Text,  StyleSheet, Button} from 'react-native'

function Exercise2() {
    const[name, setName] = useState(false);
    const changeName = () => {
        setName(!name)
    }
    return ( 
        <View>
              <Text style={name ?  styles.oneChange : styles.twoChange} >
                 {name ? 'Success' : 'False'}
              </Text>
              <Button 
                style={styles.button}
                onPress={changeName}
                title="Click me"
                color="red"
              />
        </View>
    );
}
const styles = StyleSheet.create({
    oneChange: {
        backgroundColor:'limegreen',
        fontSize: 30
    },
    twoChange: {
        fontSize: 30
    },
    button : {
        fontSize: 30
    }
})
export default Exercise2;