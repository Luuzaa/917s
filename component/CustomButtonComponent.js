import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native'


function CustomButtonComponent({text, type='PRIMARY', onPress}) {
    return ( 
        <TouchableOpacity style={[styles.container, styles[`container_${type}`]]} onPress={onPress}>
                <Text style={[styles.btn, styles[`text_${type}`]]}>{text}</Text>
        </TouchableOpacity>
     );
}

export default CustomButtonComponent;

const styles = StyleSheet.create({
      container: {
          width: "90%",
          alignItems: 'center',
          backgroundColor: '#3B71F3',
          padding: 15,
          borderRadius: 5,
          marginVertical: 5
      },

      btn : {
        fontSize: 18,
        fontWeight: 'bold',
        color: 'white'
      },

      container_Google: {
        backgroundColor: '#FAE9EA'
      },
      container_Forget: {
        backgroundColor: '#e3e3e3'
      },
      container_Facebook: {
        backgroundColor: '#E7EAF4'
      },
      container_Create: {
        backgroundColor: 'white'
      },

      text_Forget: {
        color: '#363636'
      },
      text_Facebook: {
        color: '#4765A9'
      },
      text_Google: {
        color: '#DD4D44'
      },
      text_Create: {
        color: 'gray'
      }


})
